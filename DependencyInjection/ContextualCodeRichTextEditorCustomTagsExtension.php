<?php

namespace ContextualCode\RichTextEditorCustomTagsBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Config\Resource\FileResource;

class ContextualCodeRichTextEditorCustomTagsExtension extends Extension implements PrependExtensionInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('parameters.yml');
    }

    /**
     * {@inheritdoc}
     */
    public function prepend(ContainerBuilder $container)
    {
        $container->prependExtensionConfig('assetic', array('bundles' => array('ContextualCodeRichTextEditorCustomTagsBundle')));

        // Directory where public resources are stored (relative to web/ directory).
        $container->setParameter('richtexteditorcustomtags.public_dir', 'bundles/contextualcoderichtexteditorcustomtags');
        $this->addExtensionConfig($container, 'ez_platformui', 'yui.yml');
        $this->addExtensionConfig($container, 'ez_platformui', 'css.yml');
    }

    private function addExtensionConfig(ContainerBuilder $container, $extension, $configFile)
    {
        $configFile = __DIR__ . '/../Resources/config/' . $configFile;
        $config = Yaml::parse(file_get_contents($configFile));
        $container->prependExtensionConfig($extension, $config);
        $container->addResource(new FileResource($configFile));
    }
}
