/*
 * Copyright (C) eZ Systems AS. All rights reserved.
 * For full copyright and license information view LICENSE file distributed with this source code.
 */

YUI.add('ez-alloyeditor-button-marked', function (Y) {
    "use strict";

    var AlloyEditor = Y.eZ.AlloyEditor,
        React = Y.eZ.React,
        ButtonMarked;

    /**
     * The ButtonList component represents a button to add an unordered list (ul).
     *
     * @uses AlloyEditor.ButtonCommand
     * @uses AlloyEditor.ButtonStateClasses
     *
     * @class eZ.AlloyEditor.ButtonMarked
     */
    ButtonMarked = React.createClass({displayName: "ButtonMarked",
        mixins: [
            AlloyEditor.ButtonCommand,
            AlloyEditor.ButtonStateClasses,
        ],

        statics: {
            key: 'marked'
        },

        getDefaultProps: function () {
            return {
                command: 'eZAddContent',
                modifiesSelection: true,
            };
        },

        _addMarked: function () {
            this.execCommand({
                tagName: 'div',
                content: '',
                attributes: {
                    'data-ezelement': 'marked'
                }
            });
        },

        render: function () {
            var css = "ae-button ez-ae-labeled-button" + this.getStateClasses();

            return (
                React.createElement("button", {className: css, onClick: this._addMarked, tabIndex: this.props.tabIndex},
                    React.createElement("span", {className: "ez-ae-icon ez-font-icon ez-ae-icon-marked"}),
                    React.createElement("p", {className: "ez-ae-label"}, Y.eZ.trans('Marked', {}, 'onlineeditor'))
                )
            );
        },
    });

    AlloyEditor.Buttons[ButtonMarked.key] = AlloyEditor.ButtonMarked = ButtonMarked;
});
